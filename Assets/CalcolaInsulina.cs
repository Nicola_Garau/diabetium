﻿using UnityEngine;
using System.Collections;
using TheNextFlow.UnityPlugins;
using UnityEngine.UI;
using System.Globalization;
using System;

public class CalcolaInsulina : MonoBehaviour {

    public float valoreAtteso = 120;
    public float valoreRilevato;
    public InputField campoGlicemia, campoInsulina;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        
	}

    public void Calcola()
    {
        if (campoGlicemia.text != null && campoGlicemia.text.Replace(" ", "").CompareTo("") != 0)
        {
            Debug.Log("G: " + campoGlicemia.text + " I: " + campoInsulina.text);
            valoreRilevato = float.Parse(campoGlicemia.text, CultureInfo.InvariantCulture.NumberFormat);
            Debug.Log("VR: " + valoreRilevato);
            campoInsulina.text = (3 + Math.Round((valoreRilevato - valoreAtteso) / 60)).ToString();
            Debug.Log("IN: " + (3 + Math.Round((valoreRilevato - valoreAtteso) / 60)).ToString());
        }
        else
        {
            campoInsulina.text = "";
        }
    }
}
