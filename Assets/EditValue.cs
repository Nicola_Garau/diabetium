﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class EditValue : MonoBehaviour {
    public GameObject ora, glicemia, note, insulina;
    public Text ora1, ora2;
    public Text glicemia1, note1, insulina1;
    public Text glicemia2, note2, insulina2;
    public int recordNumber;
    // Use this for initialization
    void Start () {
        
    }
	
	// Update is called once per frame
	void Update () {

    }

    public void Modifica()
    {
        switch (recordNumber)
        {
            case 1:
                ora1.text = ora.GetComponent<Text>().text;
                glicemia1.text = glicemia.GetComponent<Text>().text;
                note1.text = note.GetComponent<Text>().text;
                insulina1.text = insulina.GetComponent<Text>().text;
                break;
            case 2:
                ora2.text = ora.GetComponent<Text>().text;
                glicemia2.text = glicemia.GetComponent<Text>().text;
                note2.text = note.GetComponent<Text>().text;
                insulina2.text = insulina.GetComponent<Text>().text;
                break;
        }
    }
}
