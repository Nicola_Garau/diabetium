﻿using UnityEngine;
using System.Collections;

public class ToggleActive : MonoBehaviour {
    public GameObject obj;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}
    public void toggle()
    {
        if (obj.active) obj.SetActive(false);
        else obj.SetActive(true);
    }
}
