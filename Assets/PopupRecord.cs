﻿using UnityEngine;
using System.Collections;
using TheNextFlow.UnityPlugins;
using UnityEngine.UI;

public class PopupRecord : MonoBehaviour {
    public GameObject ora, glicemia, insulina, note;
    public Text oraN;
    public InputField glicemiaN, insulinaN, noteN;
    public GameObject pageToBeDisabled, pageToBeEnabled, confirmButton;
    public GameObject sidemenu;
    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void viewRecord(int n)
    {
        AndroidNativePopups.OpenAlertDialog(
                "Valori", "Ora: "+ ora.GetComponent<Text>().text + " Glicemia: "+glicemia.GetComponent<Text>().text + " Insulina: "+insulina.GetComponent<Text>().text + " Note: "+note.GetComponent<Text>().text,
                "Ok", "Modifica",
                () => { Debug.Log("Accept was pressed"); }, () => { editRecord(n); });
    }

    public void editRecord(int n)
    {
        sidemenu.SetActive(false);
        pageToBeDisabled.SetActive(false);
        pageToBeEnabled.SetActive(true);
        oraN.text = ora.GetComponent<Text>().text;
        glicemiaN.text = glicemia.GetComponent<Text>().text;
        noteN.text = note.GetComponent<Text>().text;
        insulinaN.text = insulina.GetComponent<Text>().text;
        confirmButton.GetComponent<EditValue>().recordNumber = n;
    }
}
