﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class ChangeColor : MonoBehaviour {

    public float R, G, B, A;
    public GameObject header;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void change()
    {
        header.GetComponent<Image>().color = new Color(R, G, B, A);
    }
}
