﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class GoToPage : MonoBehaviour {

    public GameObject headerText;
    public GameObject viewToBeDisabled;
    public GameObject viewToBeEnabled;
    public GameObject giornoDiario;
    public GameObject settimanaDiario;
    public GameObject meseDiario;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

    }

    public void Go(string destinationPage)
    {
        switch (destinationPage)
        {
            case "insert":
                headerText.GetComponent<Text>().text = "Inserisci glicemia";
                break;
            case "diary":
                headerText.GetComponent<Text>().text = "Diario";
                //giornoDiario.SetActive(true);
                //settimanaDiario.SetActive(true);
                //meseDiario.SetActive(true);
                break;
            case "share":
                headerText.GetComponent<Text>().text = "Condividi diario";
                break;
            case "home":
                headerText.GetComponent<Text>().text = "DiabetIUM";
                //giornoDiario.SetActive(false);
                //settimanaDiario.SetActive(false);
                //meseDiario.SetActive(false);
                break;
            case "month":
                headerText.GetComponent<Text>().text = "Diario mensile";
                break;
            case "week":
                headerText.GetComponent<Text>().text = "Diario settimanale";
                break;
            default:
                break;
        }

        viewToBeDisabled.SetActive(false);
        viewToBeEnabled.SetActive(true);
    }
}
