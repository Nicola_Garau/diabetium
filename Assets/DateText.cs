﻿using UnityEngine;
using System.Collections;
using TheNextFlow.UnityPlugins;
using UnityEngine.UI;

public class DateText : MonoBehaviour {

    public GameObject testo;

	// Use this for initialization
	void Start () {
        testo.GetComponent<Text>().text = "Diario del "+ System.DateTime.Now.ToString("dd/MM/yyyy");
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
