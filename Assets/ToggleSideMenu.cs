﻿using UnityEngine;
using System.Collections;

public class ToggleSideMenu : MonoBehaviour {

    public GameObject sidemenu;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void toggle()
    {
        if (sidemenu.activeSelf) {
            sidemenu.SetActive(false);
        }
        else
        {
            sidemenu.SetActive(true);
        }
    }
}
