﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class Back : MonoBehaviour {

    public GameObject headerText;
    public GameObject viewToBeDisabled;
    public GameObject viewToBeEnabled;

    // Use this for initialization
    void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    public void GoBack(string currentPage) {
        switch (currentPage)
        {
            case "insert":
                headerText.GetComponent<Text>().text = "DiabetIUM";
                break;
            default:
                break;
        }


        viewToBeDisabled.SetActive(false);
        viewToBeEnabled.SetActive(true);
    }
}
